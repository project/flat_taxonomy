<?php

namespace Drupal\flat_taxonomy;

/**
 * Flat Taxonomy Constants.
 */
final class FlatConstants {

  // Taxonomy is not flat.
  const FLAT_TAXONOMY_NORMAL = 0;
  // Taxonomy is flat.
  const FLAT_TAXONOMY_FLAT = 1;

}
